#include <stdio.h>
#include <stdlib.h>
#include "ARVB.H"
#include <string.h>

void pass(void){
    return;
}

long return_cod(char *string){
    
    long li = 0;
    short ind = 0;
    char straux[50] = "\0";
    for (int i = 0; i < 50; i++) {
        if (string[i] == '>') {
            ind = i+1;
            break;
        }
    }
    for (int i = 0; i < 50; i++) {
        if (string[ind+i] == '<') {
            break;
        }
        straux[i] = string[ind+i];
    }
    li = atol(straux);
    return li;
}

const char* return_descr(char *string){
    short ind = 0;
    char straux[100] = "\0";
    for (int i = 0; i < 50; i++) {
        if (string[i] == '>') {
            ind = i+1;
            break;
        }
    }
    for (int i = 0; i < 100; i++) {
        if (string[ind+i] == '<')
            break;
        straux[i] = string[ind+i];
    }
    char *straux2 = straux;
    return straux2;
}

float return_custo(char *string){
    float valor = 0.0;
    short ind = 0;
    char straux[50] = "\0";
    for (int i = 0; i < 50; i++) {
        if (string[i] == '>') {
            ind = i+1;
            break;
        }
    }
    for (int i = 0; i < 50; i++) {
        if (string[ind+i] == '<') 
            break;
        if(string[ind+i] == ',')
            straux[i] = '.';
        else
            straux[i] = string[ind+i];
    }
    valor = atof(straux);
    return valor;
}

void ler_xml(TipoPagina ** D){
    int dados=0;
    TipoRegistro reg;
    FILE *xml = fopen("projeto.xml", "r");
    if (xml != NULL) {
        while (!(feof(xml))) {
            char stringxml[100];
            char straux[100];
            char mark[2] = "\0";
            int ind;
            fscanf(xml, " %[^\n]s", stringxml);
            strcpy(straux, stringxml);
            for (int i = 0; i < 100; i++) {
                if(straux[i] == '>'){
                    ind = i+1;
                    break;
                }
            }
            mark[0] = straux[ind];
            strtok(straux, mark);
            if(strcmp(straux, "<Cod>")==0){
                reg.Chave = return_cod(stringxml);
                dados++;
            }
            else if (strcmp(straux, "<Descr>") == 0){
                strcpy(reg.Descr, return_descr(stringxml));
                dados++;
            }
            else if (strcmp(straux, "<Custo>") == 0){
                reg.Custo = return_cod(stringxml);
                dados++;
            }
            if (dados == 3) {
                dados = 0;
                Insere(reg, D);
            }
        }
    }
    fclose(xml);
    xml = NULL;
}

int main (int argc, char *argv[]){
    TipoRegistro reg;
    TipoPagina *D;
    Inicializa(&D);
    ler_xml(&D);
    int loop = 1;
    while (loop) {
        int op = 0;
        scanf("%d", &op);
        switch (op) {
            case 1:
                printf("Digite a etapa que deseja buscar.\nCodigo: ");
                scanf("%ld", &reg.Chave);
                Pesquisa(&reg, D);
                break;
            case 2:
                break;
            case 3:
                loop = 0; 
                break;
        }
    }
    return 0;
}

