# Disciplina: Algoritmos e Estrutura de dados II - AVALIAÇÃO 3

## Docente: MÁRCIO SOUSSA

Uma empresa de engenharia civil (XYZ) tem os dados de seus projetos processados por uma empresa parceira terceirizada. A XYZ contratou a sua equipe para ajudar na exibição e busca dessas informações por conta de uma demora percebida sempre que a XYZ solicita a árvore de entregáveis do projeto à sua empresa parceira. Por questão de eficiência, sua equipe sugere ler diretamente o arquivo XML do projeto e montar a árvore desejada. Segue abaixo o arquivo em XML com os dados do projeto:

``` xml
<?xml version="1.0" encoding="utf-8" ?>
<Entregas>
    <Entrega>
        <Cod>1</Cod>
        <Descr>Sondagem</Descr>
        <Custo>20000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>7</Cod>
        <Descr>Escavação</Descr>
        <Custo>20000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>5</Cod>
        <Descr>Baldrame</Descr>
        <Custo>15000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>6</Cod>
        <Descr>Piso</Descr>
        <Custo>10000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>13</Cod>
        <Descr>Viga</Descr>
        <Custo>7000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>17</Cod>
        <Descr>Parede</Descr>
        <Custo>12000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>12</Cod>
        <Descr>Laje</Descr>
        <Custo>8000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>22</Cod>
        <Descr>Empena</Descr>
        <Custo>10000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>28</Cod>
        <Descr>Cobertura</Descr>
        <Custo>25000,00</Custo>
    </Entrega>
    <Entrega>
        <Cod>99</Cod>
        <Descr>Forro</Descr>
        <Custo>8000,00</Custo>
    </Entrega>
</Entregas>
```

A missão da sua equipe é criar um programa que possibilite ao gestor da empresa XYZ executar as seguintes funcionalidades:

|Passo|Descrição|
|--|--|
|1|Ler o arquivo xml e extrair as informações de cada parte do projeto (código, descrição e custo) e inserir em uma árvore B (Ordem 4 - segundo Knuth)|
|2| Pesquisar uma etapa da obra pelo código e apresentar o nome e o custo. Caso não exista a etapa, informar ao gestor|
|3| Exibir todas as etapas do projeto de forma hierarquizada (código, nome e custo)|
|4| Exibir o custo total da obra|
|5| Sair do programa|